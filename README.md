# ♥♦ Four Card Challenge Section ♠♣

### 💡 Inspired me to do this challenge by... ⚡
- [Florin Pop youtube channel](https://www.youtube.com/channel/UCeU-1X402kT-JlLdAitxSMA)  
- [Workshop video](https://www.youtube.com/watch?v=PcSUEo0P0GU)

### 🏁 About this challenge
[Frontend Mentor Four cards challenge](https://www.frontendmentor.io/challenges/four-card-feature-section-weK1eFYK)

![Design preview for the Four card feature section coding challenge](./design/desktop-preview.jpg)
